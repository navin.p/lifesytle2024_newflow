//
//  DateTimePicker.swift
//  DPS
//
//  Created by Saavan Patidar on 13/02/19.
//  Copyright © 2019 Saavan. All rights reserved.
//

import UIKit

class DateTimePicker: UIViewController {
    
    var strTag = Int()
    var strDateGlobal = String()
    var strType = String()
    var minimumDate = true
    
    @IBOutlet var dateTimePicker: UIDatePicker!
    @IBOutlet weak var viewBackground: CardView!
    
    weak var handleOnSelectionOfDate: datePickerView?
    var dataFormatter = DateFormatter()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
        
        if(minimumDate)
        {
           dateTimePicker.minimumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        }
       
       
        if strType == "Date" {
            
            dateTimePicker.datePickerMode = UIDatePicker.Mode.date
            
            
        } else if strType == "DateTime" {
            
            dateTimePicker.datePickerMode = UIDatePicker.Mode.dateAndTime
            
        }else {
            
            dateTimePicker.datePickerMode = UIDatePicker.Mode.time
            
        }
        
        self.setDate()
        
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        self.dismiss(animated: false) { }
        
    }
    
    @IBAction func action_Done(_ sender: UIButton) {
        
        
        print(strDateGlobal)
        self.handleOnSelectionOfDate?.setDateOnSelction(strDate: strDateGlobal, tag: strTag)
        self.dismiss(animated: false) {}
        
    }
    
    @IBAction func datePickerValueChanged(_ sender: Any) {
        
        self.setDate()
        
    }
    
    func setDate() {
        
        let dateFormatter = DateFormatter()
        
        if strType == "Date" {
            
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.dateFormat = "dd/MM/yy"
            
            strDateGlobal = dateFormatter.string(from: dateTimePicker.date)
            
             strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "dd/MM/yy" , strFormatToBeConverted: "yyyy-MM-dd")
            
        } else if strType == "DateTime" {
            
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            strDateGlobal = dateFormatter.string(from: dateTimePicker.date)
            
            strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "MM/dd/yyyy hh:mm a" , strFormatToBeConverted: "MM/dd/yyyy HH:mm")
            
        }else {
            
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            strDateGlobal = dateFormatter.string(from: dateTimePicker.date)
            
            strDateGlobal = dateTimeConvertor(str: strDateGlobal, formet: "hh:mm a" , strFormatToBeConverted: "hh:mm a")
            
        }
        
    }
    
}
