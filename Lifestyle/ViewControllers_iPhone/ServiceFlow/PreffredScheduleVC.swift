//
//  PreffredScheduleVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import SafariServices
import MapKit
import CoreLocation




class PreffredScheduleVC: UIViewController , MKMapViewDelegate , CLLocationManagerDelegate{
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnAcceptContinue: UIButton!
    @IBOutlet weak var mapTech: MKMapView!
    
    private var locationManager: CLLocationManager!
    private var currentLocation: CLLocation?
    
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    var strLat = "0.0"
    var strLong = "0.0"
    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        tvList.tableFooterView = UIView()
        btnAcceptContinue.layer.cornerRadius =  btnAcceptContinue.frame.size.height/2
        
        
        getCurrentLocation()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
    }
    
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                                          for controller in self.navigationController!.viewControllers as Array {
                                                                                                   if controller.isKind(of: DashboardVC.self) {
                                                                                                       self.navigationController!.popToViewController(controller, animated: true)
                                                                                                       break
                                                              }
                                                              
                                                          }
                                
                                
                            })
        })
    }
    
    
    //MARK:
    //MARK: RelatedMap
    func getCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                showLocationAccessAlertAction(viewcontrol: self)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func loadDataOnMap(locations : [CLLocation])  {
        
        let letD = Double("\(strLat)")
        let longD = Double("\(strLong)")
        
        aryList = NSMutableArray()
        var dict = NSMutableDictionary()
        dict.setValue("12/21/2019\n10:00 AM - 11:00 PM", forKey: "Date")
        dict.setValue("Chris Slaughter", forKey: "TechName")
        dict.setValue("\(letD!)", forKey: "Lat")
        dict.setValue("\(longD! - 00.1)", forKey: "Long")
        dict.setValue("Techimages", forKey: "TechImage")
        dict.setValue("Clark Pest", forKey: "CompanyName")
        dict.setValue("4.5", forKey: "rating")

        
        
        
        aryList.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("01/15/2020\n10:00 AM - 11:00 PM", forKey: "Date")
        dict.setValue("Michael Urban", forKey: "TechName")
        dict.setValue("\(letD!)", forKey: "Lat")
        dict.setValue("\(longD! + 0.1)", forKey: "Long")
        dict.setValue("Techimages1", forKey: "TechImage")
        dict.setValue("ABC Houston", forKey: "CompanyName")
        dict.setValue("4.0", forKey: "rating")

        
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("01/21/2020\n10:00 AM - 11:00 PM", forKey: "Date")
        dict.setValue("Shelton Ware", forKey: "TechName")
        dict.setValue("\(letD! - 0.1)", forKey: "Lat")
        dict.setValue("\(longD!)", forKey: "Long")
        dict.setValue("Techimages2", forKey: "TechImage")
        dict.setValue("Rottler Pest", forKey: "CompanyName")
        dict.setValue("4.0", forKey: "rating")

        
        aryList.add(dict)
        
        arySelectedList.add(aryList.object(at: 0)as! NSDictionary)
        
        let allAnnotations = self.mapTech.annotations
        self.mapTech.removeAnnotations(allAnnotations)
        
        for (index, location) in aryList.enumerated() {
            print("Item \(index): \(location)")
            print(location)
            var strlet = ""
            var strLong = ""
            if ((location as AnyObject).value(forKey: "Lat") is String){
                strlet = ((location as AnyObject).value(forKey: "Lat")as! String)
            }
            if ((location as AnyObject).value(forKey: "Long") is String){
                strLong = ((location as AnyObject).value(forKey: "Long")as! String)
            }
            if (strlet != "") && (strLong != "") {
                let latitude: CLLocationDegrees = Double (strlet)!
                let longitude: CLLocationDegrees = Double (strLong)!
                let location1: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                let annotation = MKPointAnnotation()
                annotation.coordinate = location1
                annotation.title =  ""
                annotation.subtitle = "\(index)"
                mapTech.addAnnotation(annotation)
                mapTech.showAnnotations(mapTech.annotations, animated: true)
                mapTech.showsUserLocation = true
            }
        }
        
        self.tvList.reloadData()
    }
    
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
      
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let annotationIdentifier = "AnnotationIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView!.canShowCallout = true
        }
        else {
            annotationView!.annotation = annotation
        }
        print("\(String(describing: annotation.subtitle!))")
        annotationView!.image = UIImage(named: "car-top-view")
        configureDetailView(annotationView: annotationView!, tag:(annotation.subtitle as? NSString)!)
        return annotationView
        
        
    }
    
    func configureDetailView(annotationView: MKAnnotationView , tag : NSString) {
        
        let index = Int("\(tag)")!
        
        let dictData = aryList.object(at: index) as! NSDictionary
        let strDateTime = "\(dictData.value(forKey: "Date")!)"
        let TechName = "\(dictData.value(forKey: "TechName")!)"
        let TechImage = UIImage(named: "\(dictData.value(forKey: "TechImage")!)")
        let CompanyName = "\(dictData.value(forKey: "CompanyName")!)"
        
        
        
        let width = 85
        let height = 85
        let snapshotView = UIView()
        let views = ["snapshotView": snapshotView]
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        snapshotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(85)]", options: [], metrics: nil, views: views))
        
        let options = MKMapSnapshotter.Options()
        options.size = CGSize(width: width, height: height)
        options.mapType = .satelliteFlyover
        options.camera = MKMapCamera(lookingAtCenter: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
        
        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.start { snapshot, error in
            if snapshot != nil {
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
                let lbltitle = UILabel(frame: CGRect(x: 90, y: 5, width: 215, height: 25))
                let lblSubtitle = UILabel(frame: CGRect(x: 90, y: lbltitle.frame.maxY, width: 215, height: 60))
                imageView.backgroundColor = UIColor.groupTableViewBackground
                imageView.layer.cornerRadius = CGFloat(height/2)
                imageView.clipsToBounds = true
                lbltitle.numberOfLines = 2
                lblSubtitle.numberOfLines = 3
                lbltitle.font = UIFont.boldSystemFont(ofSize: 14)
                lblSubtitle.font = UIFont.systemFont(ofSize: 14)
                lbltitle.text = "Not available"
                lblSubtitle.text = "Not available"
                lblSubtitle.textColor = UIColor.darkGray
                lbltitle.text = "\(CompanyName) (\(TechName))"
                lblSubtitle.text = "\(strDateTime)"
                imageView.image = TechImage
                snapshotView.addSubview(imageView)
                snapshotView.addSubview(lbltitle)
                snapshotView.addSubview(lblSubtitle)
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.AnotationhandleTap(_:)))
            snapshotView.addGestureRecognizer(tap)
            snapshotView.isUserInteractionEnabled = true
            snapshotView.tag = index
            annotationView.detailCalloutAccessoryView = snapshotView
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let dictData = self.aryList.object(at: Int("\(view.annotation!.subtitle! ?? "0")")!)as! NSDictionary
              self.arySelectedList = NSMutableArray()
              self.arySelectedList.add(dictData)
              self.tvList.reloadData()
    }
    
    
    @objc func AnotationhandleTap(_ sender: UITapGestureRecognizer) {
        let tag = sender.view?.tag
        DispatchQueue.main.async {
            let dictData = self.aryList.object(at: tag!)as! NSDictionary
                 self.arySelectedList = NSMutableArray()
                     self.arySelectedList.add(dictData)
                 self.tvList.reloadData()
        }
    }
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        defer { currentLocation = locations.last }
        
        if currentLocation != nil {
            self.strLat = "\(currentLocation!.coordinate.latitude)"
            self.strLong = "\(currentLocation!.coordinate.longitude)"
            
            
            // Zoom to user location
            self.loadDataOnMap(locations : locations)
            locationManager.stopUpdatingLocation()
        }
    }
    func showLocationAccessAlertAction(viewcontrol : UIViewController)  {
        let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
        alert.addAction(UIAlertAction (title: "Go to setting", style: .default, handler: { (nil) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }))
        // add the actions (buttons)
        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
        }))
        viewcontrol.present(alert, animated: true, completion: nil)
    }
    
}








//MARK:
//MARK: UITableViewDelegate
extension PreffredScheduleVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "SchedulePreffredCell", for: indexPath as IndexPath) as! SchedulePreffredCell
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        
        cell.lblDateTime.text = "\(dictData.value(forKey: "Date")!)"
        cell.lblTechName.text = "\(dictData.value(forKey: "TechName")!)"
        cell.imgTech.layer.cornerRadius = cell.imgTech.frame.width/2
        cell.imgTech.layer.borderColor  = UIColor.lightGray.cgColor
        cell.imgTech.layer.borderWidth = 1.0
        cell.imgTech.image = UIImage(named: "\(dictData.value(forKey: "TechImage")!)")
        cell.ratingBar.contentMode = UIView.ContentMode.scaleAspectFit
        cell.ratingBar.type = .floatRatings
        let double = ("\(dictData.value(forKey: "rating")!)" as NSString).doubleValue
        cell.lblrating.text = "\(dictData.value(forKey: "rating")!)"
        cell.ratingBar.rating = double
        cell.lblComapnyName.text = "\(dictData.value(forKey: "CompanyName")!)"
 
        
        if(arySelectedList.contains(dictData)){
                   cell.viewRadio.backgroundColor = appThemeColor
            cell.contentView.backgroundColor = UIColor.groupTableViewBackground
            
               }else{
                   cell.viewRadio.backgroundColor = UIColor.groupTableViewBackground
            cell.contentView.backgroundColor = UIColor.white
               }
        
        
        cell.btnLink.tag = indexPath.row
        cell.btnLink.addTarget(self, action: #selector(didTapLinkButton(sender:)), for: .touchUpInside)
        
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        
        self.arySelectedList = NSMutableArray()
            self.arySelectedList.add(dictData)
        self.tvList.reloadData()
//        var anotation = MKAnnotationView()
//        for item in self.mapTech!.annotations{
//            let titletag = ((item as AnyObject)as! MKAnnotation).subtitle
//            if(Int(titletag!) == indexPath.row){
//              anotation = item as! MKAnnotationView
//            }
//        }
        self.mapTech.deselectAnnotation([self.mapTech!.annotations] as? MKAnnotation, animated: true)
      //  self.mapTech.selectAnnotation(self.mapTech.annotations[indexPath.row], animated: true)

    }
    @objc func didTapLinkButton(sender : UIButton) {
        // self.openWeb(contentLink: "https://www.google.com")
        let dictData = aryList.object(at: sender.tag)as! NSDictionary
        
        
        let dict = NSMutableDictionary()
        dict.setValue("\(dictData.value(forKey: "TechName")!)", forKey: "name")
        dict.setValue(UIImage(named: "\(dictData.value(forKey: "TechImage")!)"), forKey: "TechImage")
        
        
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let SCVC = storyboard.instantiateViewController(withIdentifier: "EmployeeProfileVC") as! EmployeeProfileVC
        SCVC.dictData = dict
        SCVC.strComeFrom = "NewFlow"
        self.navigationController?.pushViewController(SCVC, animated: true)
        
        
        
    }
    func openWeb(contentLink : String){
        let url = URL(string: contentLink)!
        let controller = SFSafariViewController(url: url)
        controller.preferredBarTintColor = UIColor.darkGray
        controller.preferredControlTintColor = UIColor.groupTableViewBackground
        controller.dismissButtonStyle = .close
        controller.configuration.barCollapsingEnabled = true
        self.present(controller, animated: true, completion: nil)
        controller.delegate = self
    }
}

//MARK:
//MARK: SFSafariViewControllerDelegate
extension PreffredScheduleVC: SFSafariViewControllerDelegate
{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

//MARK:
//MARK: UITableViewCell
class SchedulePreffredCell: UITableViewCell {
    @IBOutlet weak var viewRadio: CardView!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblTechName: UILabel!
    @IBOutlet weak var lblComapnyName: UILabel!
    @IBOutlet weak var ratingBar: FloatRatingView!
    @IBOutlet weak var lblrating: UILabel!

    @IBOutlet weak var imgTech: UIImageView!
    @IBOutlet weak var btnLink: UIButton!
}



