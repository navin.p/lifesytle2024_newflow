//
//  PropertyDetailVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class PropertyDetailVC: UIViewController {
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var btnConfirmContinue: UIButton!
 
    @IBOutlet weak var lblServicePrice: UILabel!
    @IBOutlet weak var btntaxApplicable: UIButton!

    
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        btnConfirmContinue.layer.cornerRadius =  btnConfirmContinue.frame.size.height/2
      
        
    }
    
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    
    @IBAction func actionOnConfirm(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                
                                let dictServiceData = nsud.value(forKey: "LifeStyle_ServiceSelection")as! NSDictionary
                                
                                // Warranty FLow
                                
                                if( "\(dictServiceData.value(forKey: "id")!)" == "1"){
                                    let alert = UIAlertController(title: "This issue is covered under service warranty. Would you like to report it now?", message: "", preferredStyle: UIAlertController.Style.alert)
                                    
                                    
                                    alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { action in
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let SCVC = storyboard.instantiateViewController(withIdentifier: "SurveyComplainVC") as! SurveyComplainVC
                                        SCVC.dictWO  = NSDictionary()
                                        SCVC.strTitle = "Service Request"
                                        
                                        self.navigationController?.pushViewController(SCVC, animated: true)
                                        
                                    }))
                                    
                                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { action in
                                        for controller in self.navigationController!.viewControllers as Array {
                                            if controller.isKind(of: DashboardVC.self) {
                                                self.navigationController!.popToViewController(controller, animated: true)
                                                break
                                            }
                                        }
                                        
                                        
                                    }))
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                    
                                }
                                    
                                    
                                    // New Sales Flow
                                else if( "\(dictServiceData.value(forKey: "id")!)" == "2"){
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let SCVC = storyboard.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                                    self.navigationController?.pushViewController(SCVC, animated: true)
                                    
                                }
                                    
                                    // Uberization Flow
                                else if( "\(dictServiceData.value(forKey: "id")!)" == "3"){
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    
                                    let SCVC = storyboard.instantiateViewController(withIdentifier: "PricingVC") as! PricingVC
                                    self.navigationController?.pushViewController(SCVC, animated: true)
                                }
                                
                            })
        })
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func actionOnEditAddress(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                                
                                
                            })
        })
    }
    
    
  
    
}


//MARK:
//MARK: UITextFieldDelegate's
extension PropertyDetailVC : UITextFieldDelegate{
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
