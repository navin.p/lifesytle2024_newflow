//
//  ServiceDetailVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class ServiceDetailVC: UIViewController {
    
    
    //MARK:
    //MARK: IBOutlet's
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var imageTaken: UIImageView!
    @IBOutlet weak var imageDataBase: UIImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    
    //MARK:
    //MARK: LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        let dictServiceData = nsud.value(forKey: "LifeStyle_ServiceSelection")as! NSDictionary
        lblServiceName.text = "\(dictServiceData.value(forKey: "title")!)"
        imageTaken.image =   UIImage(named: "\(dictServiceData.value(forKey: "image")!)")
        
        if(nsud.value(forKey: "LifeStyle_ServiceImage") != nil){
         let dict = nsud.value(forKey: "LifeStyle_ServiceImage")as! NSDictionary
            let imgData = dict.value(forKey: "DatabaseServiceImage")as? Data
            if(imgData?.count != 0){
                imageDataBase.image = UIImage(data: imgData!)
                lblServiceName.text = "\(dict.value(forKey: "DatabaseServiceImageName")!)"

            }
            let imgData1 = dict.value(forKey: "CaptureServiceImage")as? Data
                      if(imgData1?.count != 0){
                          imageTaken.image = UIImage(data: imgData1!)

                      }
            
        }
        
        btnContinue.layer.cornerRadius =  btnContinue.frame.size.height/2
        imageTaken.layer.cornerRadius =  5.0
        imageDataBase.layer.cornerRadius =  5.0
        imageTaken.layer.borderColor =  UIColor.lightGray.cgColor
        imageDataBase.layer.borderColor =  UIColor.lightGray.cgColor
        imageTaken.layer.borderWidth =  1.0
        imageDataBase.layer.borderWidth =  1.0
        tvList.tableFooterView = UIView()
    }
    
    //MARK:
    //MARK: IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    @IBAction func actionOnContinue(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "PropertyDetailVC") as! PropertyDetailVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                            })
        })
    }
}

//MARK:
//MARK: UITableViewDelegate
extension ServiceDetailVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath as IndexPath) as! ServiceCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:
//MARK: UITableViewCell
class ServiceCell: UITableViewCell {
    
    
}
