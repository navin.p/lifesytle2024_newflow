//
//  TakeImageVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/10/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import TransitionButton

class TakeImageVC: UIViewController {
    
    
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var btnContinue: TransitionButton!
    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnGallery: UIButton!
    
    var dictImageForService = NSMutableDictionary()
    
    //MARK:
    //MARK : Variable
    var imagePicker: UIImagePickerController!
    enum ImageSource {
        case photoLibrary
        case camera
    }
    //MARK:
    //MARK : LifeCycle's
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.layer.cornerRadius =  btnContinue.frame.size.height/2
        imageService.layer.cornerRadius = 6.0
        btnCamera.layer.cornerRadius =  5.0
        btnGallery.layer.cornerRadius =  5.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        btnContinue.layer.cornerRadius =  btnContinue.frame.size.height/2
        nsud.set(nil, forKey: "LifeStyle_ServiceImage")
        nsud.set(nil, forKey: "LifeStyle_ServiceSelection")

    }
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    @IBAction func actionOnContinue(_ sender: UIButton) {
        
        if(self.imageService.image != nil){
            
            if(isInternetAvailable())
            {
                
                self.btnContinue.startAnimation()
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    let dictimageResult = self.uploadTest()
                    self.btnContinue.stopAnimation(animationStyle: .normal
                        , revertAfterDelay: 2.0, completion: {
                            if(dictimageResult.count != 0){
                                let imageData = self.imageService.image!.jpegData(compressionQuality: 1.0)
                                self.dictImageForService = NSMutableDictionary()
                                self.dictImageForService.setValue(imageData, forKey: "CaptureServiceImage")
                                let  value = dictimageResult.value(forKey: "image")as! UIImage
                                
                                let imageDataDatabase = value.jpegData(compressionQuality: 1.0)
                                self.dictImageForService.setValue(imageData, forKey: "CaptureServiceImage")
                                self.dictImageForService.setValue(imageDataDatabase, forKey: "DatabaseServiceImage")
                                self.dictImageForService.setValue("\(dictimageResult.value(forKey: "name")!)", forKey: "DatabaseServiceImageName")
                                nsud.set(self.dictImageForService, forKey: "LifeStyle_ServiceImage")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "ServiceSelectionVC") as! ServiceSelectionVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                            }else{
                                let imageData = self.imageService.image!.jpegData(compressionQuality: 1.0)
                                
                                self.dictImageForService = NSMutableDictionary()
                                self.dictImageForService.setValue(imageData, forKey: "CaptureServiceImage")
                                self.dictImageForService.setValue(Data(), forKey: "DatabaseServiceImage")
                                self.dictImageForService.setValue("", forKey: "DatabaseServiceImageName")
                                nsud.set(self.dictImageForService, forKey: "LifeStyle_ServiceImage")
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "ServiceSelectionVC") as! ServiceSelectionVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                            }
                            
                    })
                }
                
            }
                
            else{
                
                let imageData = self.imageService.image!.jpegData(compressionQuality: 1.0)
                
                dictImageForService = NSMutableDictionary()
                dictImageForService.setValue(imageData, forKey: "CaptureServiceImage")
                dictImageForService.setValue(Data(), forKey: "DatabaseServiceImage")
                dictImageForService.setValue("", forKey: "DatabaseServiceImageName")
                nsud.set(dictImageForService, forKey: "LifeStyle_ServiceImage")
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let SCVC = storyboard.instantiateViewController(withIdentifier: "ServiceSelectionVC") as! ServiceSelectionVC
                self.navigationController?.pushViewController(SCVC, animated: true)
            }
            
  
            
        }else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please take picture!", viewcontrol: self)
            
            
        }
        
        
        
        
        
    }
    
    @IBAction func actionOnCamera(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
    @IBAction func actionOnGallery(_ sender: UIButton) {
        selectImageFrom(.photoLibrary)
    }
    
    
    //MARK:
    //MARK: Extra Function
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    
    
    func uploadTest()-> NSMutableDictionary{
        
        let boundary = "---------------------------14737809831466499882746641449"
        
        let imageData = imageService.image!.jpegData(compressionQuality: 0.1)
        
        let urlString = "\("\(URL_UploadImage)")"
        
        let request = NSMutableURLRequest()
        
        request.url = URL(string: urlString)
        
        request.httpMethod = "POST"
        
        
        let contentType = "multipart/form-data; boundary=\(boundary)"
        
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        var body = Data()
        if let data = "\r\n--\(boundary)\r\n".data(using: .utf8) {
            body.append(data)
        }
        let d = Date()
        let df = DateFormatter()
        df.dateFormat = "MMddyyyyHmssSSSS"
        if let data = "Content-Disposition: form-data; name=\"file\"; filename=\"\("\(df.string(from: d)).jpg" )\"\r\n".data(using: .utf8) {
            body.append(data)
        }
        
        if let data = "Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8) {
            body.append(data)
        }
        
        if let imageData = imageData {
            
            body.append(imageData)
        }
        
        if let data = "\r\n--\(boundary)--\r\n".data(using: .utf8) {
            body.append(data)
        }
        
        request.httpBody = body
        
        var returnData: Data? = nil
        do {
            returnData = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: nil)
        } catch {
            
            return NSMutableDictionary()
        }
        
        
        var error: Error?
        var json: [AnyHashable : Any]? = nil
        do {
            if let returnData = returnData {
                json = try JSONSerialization.jsonObject(with: returnData, options: []) as? [AnyHashable : Any]
            }else{
                
                return NSMutableDictionary()
            }
        } catch {
            
            return NSMutableDictionary()
        }
        
        var strImageData =  ""
        var decodedimage: UIImage? = nil
        if let value = json?["image"] {
            strImageData =  String("\(value)".dropFirst(2))
            strImageData =  String("\(strImageData)".dropLast(1))
            
            print(strImageData)
            
            if let decodedData = Data(base64Encoded: "\(strImageData)", options: .ignoreUnknownCharacters) {
                let image = UIImage(data: decodedData)
                decodedimage = image
                
            }
            
        }
        var strImageName: String? = nil
        if let value = json?["name"] {
            strImageName = "\(value)"
        }
        let dictImageData = NSMutableDictionary()
        dictImageData.setValue(decodedimage, forKey: "image")
        dictImageData.setValue(strImageName, forKey: "name")
        return dictImageData
    }
    
}

//MARK:
//MARK: UIImagePickerControllerDelegate
extension TakeImageVC: UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        imageService.image = selectedImage
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}

extension String {
    func base64ToImage(_ base64String: String) -> UIImage? {
        guard let imageData = Data(base64Encoded: base64String) else { return nil }
        return UIImage(data: imageData)
    }
}
