//
//  ServiceSelectionVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/12/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class ServiceSelectionVC: UIViewController {
    //MARK:
    //MARK : IBOutlet's
    @IBOutlet weak var tvList: UITableView!
    @IBOutlet weak var btnAcceptContinue: UIButton!
    
    var aryList = NSMutableArray()
    var arySelectedList = NSMutableArray()
    
    //MARK:
    //MARK : LifeCycle's
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        var dict = NSMutableDictionary()
        dict = NSMutableDictionary()
        dict.setValue("Carpenter Ant", forKey: "title")
        dict.setValue("CarpenterAnt", forKey: "image")
        dict.setValue(1, forKey: "id")
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Crazy Ant", forKey: "title")
        dict.setValue("CrazyAnt", forKey: "image")
        dict.setValue(2, forKey: "id")
        aryList.add(dict)
        
        dict = NSMutableDictionary()
        dict.setValue("Fire Ant", forKey: "title")
        dict.setValue("fireant", forKey: "image")
        dict.setValue(3, forKey: "id")
        aryList.add(dict)
        
        let arytemp = self.aryList.sorted(by: { (Obj1, Obj2) -> Bool in
            let Obj1_Name = (Obj1 as AnyObject).title ?? ""
            let Obj2_Name = (Obj2 as AnyObject).title ?? ""
            return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
        })
        self.aryList = NSMutableArray()
        self.aryList = (arytemp as NSArray).mutableCopy()as! NSMutableArray
        tvList.tableFooterView = UIView()
        btnAcceptContinue.layer.cornerRadius =  btnAcceptContinue.frame.size.height/2
        arySelectedList.add(aryList.object(at: 0))
        nsud.setValue(aryList.object(at: 0)as! NSDictionary, forKey: "LifeStyle_ServiceSelection")
        
    }
    
    //MARK:
    //MARK : IBAction's
    @IBAction func actionOnBack(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                self.navigationController?.popViewController(animated: true)
                            })
        })
    }
    @IBAction func actionOnContinue(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let SCVC = storyboard.instantiateViewController(withIdentifier: "ServiceDetailVC") as! ServiceDetailVC
                                self.navigationController?.pushViewController(SCVC, animated: true)
                                
                                
                            })
        })
    }
    
}
//MARK:
//MARK: UITableViewDelegate
extension ServiceSelectionVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvList.dequeueReusableCell(withIdentifier: "ServiceSelectionCell", for: indexPath as IndexPath) as! ServiceSelectionCell
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblTitle.text = "\(dictData.value(forKey: "title")!)"
        
        if(arySelectedList.contains(aryList.object(at: indexPath.row))){
            cell.viewRadio.backgroundColor = appThemeColor
        }else{
            cell.viewRadio.backgroundColor = UIColor.groupTableViewBackground
        }
        
        cell.imgService.layer.cornerRadius = 4.0
        cell.imgService.image = UIImage(named: "\(dictData.value(forKey: "image")!)")
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        arySelectedList = NSMutableArray()
        arySelectedList.add(aryList.object(at: indexPath.row))
        self.tvList.reloadData()
        let dictData = aryList.object(at: indexPath.row)as! NSDictionary
        nsud.setValue(dictData, forKey: "LifeStyle_ServiceSelection")
        
    }
    
}


//MARK:
//MARK: UITableViewCell
class ServiceSelectionCell: UITableViewCell {
    
    @IBOutlet weak var viewRadio: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgService: UIImageView!
    
}
