//
//  TermsConditionVC.swift
//  Lifestyle
//
//  Created by Navin Patidar on 12/13/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

class TermsConditionVC: UIViewController {
       //MARK:
       //MARK : IBOutlet's
       @IBOutlet weak var txtContain: UITextView!
    @IBOutlet weak var lblTitle: UITextView!

       var strContain = String()
    var strTitle = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        txtContain.text = strContain
        lblTitle.text = strTitle

    }
    //MARK:
     //MARK : IBAction's
     @IBAction func actionOnBack(_ sender: UIButton) {
         
         UIButton.animate(withDuration: 0.2,
                          animations: {
                             sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
         },
                          completion: { finish in
                             UIButton.animate(withDuration: 0.2, animations: {
                                 sender.transform = CGAffineTransform.identity
                                 self.navigationController?.popViewController(animated: true)
                             })
         })
     }
     

  

}
