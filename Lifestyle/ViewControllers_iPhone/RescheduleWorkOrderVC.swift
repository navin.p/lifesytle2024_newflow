//
//  RescheduleWorkOrderVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 15/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import TransitionButton
class RescheduleWorkOrderVC: UIViewController, UITextViewDelegate {

    // outlets
    @IBOutlet weak var txtviewMsg: UITextView!
    
    @IBOutlet weak var btnSave: TransitionButton!
    
    // variables
    var dictWO = NSDictionary()
    
    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        txtviewMsg.text = "Message..."
        txtviewMsg.textColor = UIColor.lightGray
        txtviewMsg.layer.borderWidth = 1.0
        txtviewMsg.layer.borderColor = borderColor
        txtviewMsg.layer.cornerRadius = 2.0
        
        btnSave.cornerRadius =  btnSave.frame.size.height/2
    }
    
   // MARK: UIButton's action
    @IBAction func actionOnSave(_ sender: Any) {
      
        if(txtviewMsg.textColor == UIColor.lightGray || txtviewMsg.text.count == 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "please enter message", viewcontrol: self)
        }
        else{// call reschedule api
            
            if(isInternetAvailable()){
                rescheduleUpcomingWO()
            }
            else
            {
                 showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
            }
        }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
   
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DocumentsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TransactionsVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    @IBAction func actionOnPastService(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is WorkOrdersVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
            
    }
    
    @IBAction func actionOnTask(_ sender: UIButton) {
        
        var vcFound = false
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is TaskVC)
            {
                vcFound = true
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
        if(vcFound == false)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
        self.navigationController?.pushViewController(docVC, animated: true)
        }
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    
    // MARK: UITextField delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Message..."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Message..."
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, set
            // the text color to black then set its text to the
            // replacement string
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.textColor = UIColor.black
            textView.text = text
        }
            
            
            // For every other case, the text should change with the usual
            // behavior...
        else {
            return true
        }
        
        // ...otherwise return false since the updates have already
        // been made
        return false
    }
    
    // MARK: Web Service
    func rescheduleUpcomingWO(){
            
            let strUrl = BaseURL+"SendLifeStyleRescheduleEmailForUpComingServicesAsync?companyKey=\(dictWO.value(forKey: "CompanyKey")!)&message=\(txtviewMsg.text!)&workorderNo=\(dictWO.value(forKey: "WorkOrderNo")!)&accountno=\(dictWO.value(forKey: "AccountNo")!)&AppKey=\(dictWO.value(forKey: "AppKey")!)"
        
//        FTIndicator.showProgress(withMessage: "please wait...", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        btnSave.startAnimation()
        
        WebService.callAPIBYGET(parameter: NSDictionary(), responseType: "", url: strUrl) { (response, status) in
            
//            FTIndicator.dismissProgress()
            
            customeDotLoaderRemove()
            self.btnSave.stopAnimation(animationStyle: .normal, revertAfterDelay: 1.0, completion: nil)
            if(status == "success")
            {
                if(("\(response.value(forKey: "Result")!)" == "true") || ("\(response.value(forKey: "Result")!)" == "True") || ("\(response.value(forKey: "Result")!)" == "TRUE") || "\(response.value(forKey: "Result")!)" == "1")
                {
                    let alertController = UIAlertController(title: "Message", message: "Reschedule request submitted successfully", preferredStyle: .alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                        
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                }
            }
            else{
                
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
            }
        }
        
        
        
        
        
//            WebService.callAPIBYGETWhoseResponseIsString(parameter: NSDictionary(), url: strUrl) { (response, status) in
//
//                FTIndicator.dismissProgress()
//                if(status == "success")
//                {
//
//                    if(("\(response)" == "true") || ("\(response)" == "True") || ("\(response)" == "TRUE"))
//                    {
//                        let alertController = UIAlertController(title: "Message", message: "Reschedule request submitted successfully", preferredStyle: .alert)
//
//                        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
//
//                            self.navigationController?.popViewController(animated: true)
//                        }))
//                        self.present(alertController, animated: true, completion: nil)
//                    }
//                    else{
//                        showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
//                    }
//                }
//                else{
//                   showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
//                }
//            }
        }
}
