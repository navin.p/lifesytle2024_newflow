//
//  LoginVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 25/02/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import TransitionButton
class LoginVC: UIViewController {

    // outlets
    
    @IBOutlet weak var tfEmail: ACFloatingTextfield!
    @IBOutlet weak var tfPassword: ACFloatingTextfield!
    @IBOutlet weak var btnRememberMe: UIButton!
   
    @IBOutlet weak var btnLogin: TransitionButton!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnRegisterNow: UIButton!
    
    @IBOutlet weak var imgviewLogo: UIImageView!
    
    // MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
     // set padding to text field
      tfEmail.setLeftPaddingPoints(5.0)
      tfPassword.setLeftPaddingPoints(5.0)
      btnLogin.cornerRadius = btnLogin.frame.size.height/2
      btnRememberMe.setImage(UIImage(named: uncheck), for: .normal)
      addAnimationOnView(view: imgviewLogo)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            if(nsud.value(forKey: "isRemember") as? Bool == true)
            {
                tfEmail.text = "\(nsud.value(forKey: "email")!)"
                tfPassword.text = "\(nsud.value(forKey: "password")!)"
                btnRememberMe.setImage(UIImage(named: check), for: .normal)
            }
            else{
                tfEmail.text = ""
                tfPassword.text = ""
                btnRememberMe.setImage(UIImage(named: uncheck), for: .normal)
            }
            
    }
    
    // MARK: UIButton action
  
    @IBAction func action_RememberMe(_ sender: UIButton) {
        
        if(btnRememberMe.currentImage == UIImage(named: uncheck))
        {
            btnRememberMe.setImage(UIImage(named: check), for: .normal)
        }
        else
        {
            btnRememberMe.setImage(UIImage(named: uncheck), for: .normal)
        }
    }
    
    @IBAction func action_Login(_ sender: UIButton) {
   
        
        let strAlertMsg = self.validateFields()
        
        if(strAlertMsg.count > 0)
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage:strAlertMsg , viewcontrol: self)
        }
        else
        {// login from firebase
            
            self.login()
        }
        
//        UIButton.animate(withDuration: 0.2,
//                         animations: {
//                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
//        },
//                         completion: { finish in
//                            UIButton.animate(withDuration: 0.2, animations: {
//                                sender.transform = CGAffineTransform.identity
//
//                                let strAlertMsg = self.validateFields()
//
//                                if(strAlertMsg.count > 0)
//                                {
//                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage:strAlertMsg , viewcontrol: self)
//                                }
//                                else
//                                {// login from firebase
//
//                                    self.login()
//                                }
//                            })
//        })
        
        
        
        
       
    }
    
    @IBAction func action_ForgotPassword(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Forgot Password", message: "Please enter email", preferredStyle: .alert)
      
        alertController.addAction(UIAlertAction(title: "Submit", style: .default, handler: { alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            print(textField)
            // do something with textField
            if(textField.text?.count == 0)
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: "Please enter email", viewcontrol: self)
            }
            else
            {// call forgot password api
                self.forgotPassword(strEmail: textField.text!)
                self.dismiss(animated: true, completion: nil)
                
            }
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email"
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func action_RegisterNow(_ sender: UIButton) {
        // navigate to sign up screen
    }
    
    // MARK: Validation
    func validateFields() -> String {
        var strMsg = ""
     
        if(tfEmail.text?.count == 0)
        {
            strMsg = "Please enter email"
            return strMsg
        }
        if(validateEmail(email: tfEmail.text!) == false)
        {
            strMsg = "Please enter valid email"
            return strMsg
        }
        if(tfPassword.text?.count == 0)
        {
            strMsg = "Please enter password"
            return strMsg
        }
        
        return strMsg
    }
    
    // MARK:- Web Services
    
    // MARK:- Login
    func login()
    {
        if(isInternetAvailable())
        {
//            FTIndicator.showProgress(withMessage: "Login...", userInteractionEnable: false)
            
           // customDotLoaderShowOnFull(message: "", controller: self)
            
            btnLogin.startAnimation()
         
            Auth.auth().signIn(withEmail: tfEmail.text!, password: tfPassword.text!) { (user, error) in
                
                if(error ==  nil)
                {
                    if let user = user
                    {
                        print(user)
                        
                        if(self.btnRememberMe.currentImage == UIImage(named: check))
                        {
                            nsud.setValue(true, forKey: "isRemember")
                        }
                        nsud.setValue(self.tfEmail.text, forKey: "email")
                        nsud.setValue(self.tfPassword.text, forKey: "password")
                        nsud.synchronize()
                        
                        let datab = FirebaseDB()
                        datab.getUserAccounts(completinHandler: { (status) in
                            
//                            FTIndicator.dismissProgress()
                          //   customeDotLoaderRemove()
                          
                            self.tfEmail.text = ""
                            self.tfPassword.text = ""
                            self.btnRememberMe.setImage(UIImage(named: uncheck), for: .normal)
                            
                            self.btnLogin.stopAnimation(animationStyle: .expand
                                , revertAfterDelay: 2.0, completion: {

                                    if(status == true)
                                    {
                                        self.performSegue(withIdentifier: "DashboardVC", sender: nil)
                                    }
                                    else
                                    {// some error while getting user account
                                        self.performSegue(withIdentifier: "DashboardVC", sender: nil)
                                    }
                            })
                           
                        })                       
                    }
                }
                else
                {
//                    FTIndicator.dismissProgress()
                   // customeDotLoaderRemove()
                    self.btnLogin.stopAnimation(animationStyle: .shake, revertAfterDelay: 1.0, completion: nil)
                  
                    if let errCode = AuthErrorCode(rawValue: error!._code) {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: getFirebaseErrorMessage(errorCode: errCode), viewcontrol: self)
                    }
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }

    // MARK:- Forgot Password
    func forgotPassword(strEmail : String)
    {
        if(isInternetAvailable())
        {
//            FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
             customDotLoaderShowOnFull(message: "", controller: self)

            Auth.auth().sendPasswordReset(withEmail: strEmail) { (error) in
                
//                FTIndicator.dismissProgress()
                customeDotLoaderRemove()
                if(error == nil)
                {// show message that password has been sent to email address
                  
                     showAlertWithoutAnyAction(strtitle: "Message", strMessage: "Password reset link has been sent to your email id.", viewcontrol: self)
                }
                else
                {
                    if let errCode = AuthErrorCode(rawValue: error!._code) {
                        
                        showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: getFirebaseErrorMessage(errorCode: errCode), viewcontrol: self)
                    }
                }
            }
        }
        else
        {
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
    }
}

extension LoginVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(string == " ")
        {
            return false
        }
        return true
    }
    
}


