//
//  AddTaskVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 08/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit

//MARK:- -----------------------------------Tableview Protocol Delegate Methods -----------------------------------
protocol refreshEditopportunity : class{
    
    func refreshViewOnEditOpportunitySelection(str : String)
    
}

extension AddTaskVC: refreshEditopportunity {
    
    func refreshViewOnEditOpportunitySelection(str : String) {
        self.view.endEditing(true)
        btnStatus.setTitle(str, for: .normal)
        strStatus = str
    }
}
//MARK: - -----------------------------------Date Picker Protocol-----------------------------------

protocol datePickerView : class{
    
    func setDateOnSelction(strDate : String ,tag : Int)
    
}

extension AddTaskVC: datePickerView {
    
    func setDateOnSelction(strDate: String, tag: Int) {
        
        self.view.endEditing(true)
        if(tag == 1)
        {
            
            let strTempDate = strDate
                //dateTimeConvertor(str: strDate, formet: "yyyy-dd-MM", strFormatToBeConverted: "yyyy-MM-dd")
           
            
            if(strTempDate.count == 0)
            {
                btnDueDate.setTitle("", for: .normal)
                strDueDate = ""
            }
            else{
                btnDueDate.setTitle(strTempDate, for: .normal)
                strDueDate = strTempDate
            }
           
        }
        else if(tag == 2)
        {
             let strTempDate = strDate
               // dateTimeConvertor(str: strDate, formet: "yyyy-dd-MM", strFormatToBeConverted: "yyyy-MM-dd")
           
            if(strTempDate.count == 0)
            {
                btnReminderDate.setTitle("", for: .normal)
                strReminderDate = ""
            }
            else{
                btnReminderDate.setTitle(strTempDate, for: .normal)
                strReminderDate = strTempDate
            }
        }
    }
}
class AddTaskVC: UIViewController {
   
    // outlets
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var txtfldTitle: ACFloatingTextfield!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var btnDueDate: ButtonWithShadow!
    
    @IBOutlet weak var btnStatus: ButtonWithShadow!
    @IBOutlet weak var btnReminderDate: ButtonWithShadow!
    
    @IBOutlet weak var btnSave: ButtonWithShadow!
    @IBOutlet weak var btnCancel: ButtonWithShadow!
    
    
    // variables
    private  var strDueDate = ""
    private var strStatus = ""
    private var strReminderDate = ""
    var strTitle = ""
    var dictTask = NSDictionary()
    var arrStatus = NSMutableArray()
    //MARK: View's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        txtViewDescription.text = "Description"
        txtViewDescription.textColor = UIColor.lightGray
        txtViewDescription.layer.borderWidth = 1.0
        txtViewDescription.layer.borderColor = borderColor
        txtViewDescription.layer.cornerRadius = 2.0
        
        btnDueDate.layer.borderWidth = 1.0
        btnDueDate.layer.borderColor = borderColor
        btnDueDate.layer.cornerRadius = 2.0
        
        btnStatus.layer.borderWidth = 1.0
        btnStatus.layer.borderColor = borderColor
        btnStatus.layer.cornerRadius = 2.0

        btnReminderDate.layer.borderWidth = 1.0
        btnReminderDate.layer.borderColor = borderColor
        btnReminderDate.layer.cornerRadius = 2.0

        btnCancel.layer.cornerRadius =  btnCancel.frame.size.height/2
        btnCancel.layer.masksToBounds = true
        
        btnSave.layer.cornerRadius =  btnCancel.frame.size.height/2
        btnSave.layer.masksToBounds = true
        
        
        if(dictTask.count > 0)
        {
            lblTitleHeader.text = "Edit Task"
            txtfldTitle.text = "\(dictTask.value(forKey: "title")!)"
            txtViewDescription.text = "\(dictTask.value(forKey: "description")!)"
            txtViewDescription.textColor = UIColor.black
          
            btnDueDate.setTitle("\(dictTask.value(forKey: "duedate")!)", for: .normal)
           
            btnStatus.setTitle("\(dictTask.value(forKey: "status")!)", for: .normal)
         
            btnReminderDate.setTitle("\(dictTask.value(forKey: "reminderdate")!)", for: .normal)
           
            strDueDate = "\(dictTask.value(forKey: "duedate")!)"
            strReminderDate = "\(dictTask.value(forKey: "reminderdate")!)"
            strStatus = "\(dictTask.value(forKey: "status")!)"
        }
        else
        {
            lblTitleHeader.text = "Add Task"
        }
        
        arrStatus = ["Not Started", "In Progress", "Completed"]
    }

    // MARK: UIButton action
    
    @IBAction func actionOnDueDate(_ sender: Any) {
        self.view.endEditing(true)
        gotoDatePickerView(sender: sender as! UIButton, strType: "Date")
    }
    
    @IBAction func actionOnStatus(_ sender: Any) {
        self.view.endEditing(true)
        gotoPopViewWithArray(sender: sender as! UIButton , aryData: arrStatus, strTitle: "")
    }
    
    @IBAction func actionOnReminderDate(_ sender: Any) {
        self.view.endEditing(true)
        gotoDatePickerView(sender: sender as! UIButton, strType: "Date")
    }
    @IBAction func actionOnCancel(_ sender: Any) {
        
        self.view.endEditing(true)
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                                self.navigationController?.popViewController(animated: true)

                            })
        })
    }
    
    @IBAction func actionOnSave(_ sender: Any) {
        
        self.view.endEditing(true)
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                (sender as! UIButton).transform = CGAffineTransform.identity
                                
                                let strMsg = self.validateFields()
                                if(strMsg.count == 0)
                                {
                                    let dictAddTask = NSMutableDictionary()
                                    
                                    dictAddTask.setValue(self.txtfldTitle.text?.trimmingCharacters(in: CharacterSet.whitespaces), forKey: "title")
                                    
                                    dictAddTask.setValue((self.txtViewDescription.textColor == UIColor.lightGray) ? "" : self.txtViewDescription.text.trimmingCharacters(in: CharacterSet.whitespaces), forKey: "description")// bcoz light grey color is for placeholder
                                    
                                    dictAddTask.setValue(self.strDueDate, forKey: "duedate")
                                    dictAddTask.setValue(self.strStatus, forKey:"status")
                                    dictAddTask.setValue(self.strReminderDate, forKey: "reminderdate")
                                    if(self.dictTask.count > 0)
                                    {
                                      dictAddTask.setValue("\(self.dictTask.value(forKey: "uniquekey")!)", forKey: "uniquekey")
                                    }
                                    if(isInternetAvailable())
                                    {
                                        if(self.strTitle == "Edit Task")
                                        {
                                            if(isInternetAvailable()){
                                                self.updateTask(dictData: dictAddTask)
                                            }
                                            
                                        }
                                        else
                                        {
                                            
                                            self.addTask(dictData: dictAddTask)
                                        }
                                    }
                                    else
                                    {
                                       showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
                                    }
                                   
                                    
                                }
                                else
                                {
                                    showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: strMsg, viewcontrol: self)
                                }
                                
                            })
        })
        
        
      
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: PopUpView method
    
    func gotoPopViewWithArray(sender: UIButton , aryData : NSMutableArray , strTitle: String)  {
        let vc: PopUpView = self.storyboard!.instantiateViewController(withIdentifier: "PopUpView") as! PopUpView
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleSelectionOnEditOpportunity = self
        vc.aryTBL = arrStatus
        self.present(vc, animated: false, completion: {})
        
    }
    
    func gotoDatePickerView(sender: UIButton, strType:String)  {
        
        let vc: DateTimePicker = self.storyboard!.instantiateViewController(withIdentifier: "DateTimePicker") as! DateTimePicker
        vc.strTag = sender.tag
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.handleOnSelectionOfDate = self
        vc.strType = strType
        self.present(vc, animated: true, completion: {})
        
    }
    
    // MARK: Validation

    func validateFields()-> String
    {
        let strMsg = ""
        if(txtfldTitle.text?.count == 0)
        {
            return "Please enter title"
        }
        else if(strDueDate.count == 0)
        {
            return "Please select due date"
        }
        else  if(strStatus.count == 0)
        {
            return "Please enter status"
        }
        else if(strReminderDate.count == 0)
        {
            return "Please select reminder date"
        }
       
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dueDate = dateFormatter.date(from:strDueDate)!
        let reminderDate = dateFormatter.date(from:strReminderDate)!
        if(reminderDate>dueDate)
        {
            return "Reminder date should be less than due date"
        }
        
        return strMsg
    }
    
    // MARK: Web Services
    
    func addTask(dictData : NSMutableDictionary)
    {
//         FTIndicator.showProgress(withMessage: "Adding Task...", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
          db.addTask(dictTask: dictData) { (status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            if(status == true)
            {
                let alertController = UIAlertController(title: "Message", message: "Task added successfully", preferredStyle: .alert)
                
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                    
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage:alertSomeError , viewcontrol: self)
            }
        }
    }
    func updateTask(dictData : NSMutableDictionary)
    {        
//        FTIndicator.showProgress(withMessage: "Updating Task...", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)

        db.updateTask(dictTask: dictData) { (status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            if(status == true)
            {
                let alertController = UIAlertController(title: "Message", message: "Task updated successfully", preferredStyle: .alert)
                
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) in
                    
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            else
            {
                showAlertWithoutAnyAction(strtitle: alertMessage, strMessage:alertSomeError , viewcontrol: self)
            }
        }
    }
}

// MARK: Extension

extension AddTaskVC:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, set
            // the text color to black then set its text to the
            // replacement string
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.textColor = UIColor.black
            textView.text = text
        }
            
            // For every other case, the text should change with the usual
            // behavior...
        else {
            return true
        }
        
        // ...otherwise return false since the updates have already
        // been made
        return false
    }
}
extension AddTaskVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(string == " ")
        {
            if(textField.text?.count == 0)
            {
                return false
            }
            else
            {
                return true
            }
        }
        return true
        
    }
}
