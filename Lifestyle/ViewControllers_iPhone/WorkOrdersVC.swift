//
//  WorkOrdersVC.swift
//  Lifestyle
//
//  Created by Akshay Hastekar on 29/03/19.
//  Copyright © 2019 Akshay Hastekar. All rights reserved.
//

import UIKit
import Alamofire
import SafariServices

class WorkOrdersVC: UIViewController {

    // outlets
    @IBOutlet weak var tblviewWorkOrders: UITableView!
    
    @IBOutlet weak var constLeadingView: NSLayoutConstraint!
    
    // variables
    var type = "upcoming"
    var arrayWorkOrders = NSMutableArray()// this is the main array which contains all the data
    var arrayWorkOrdersCopy = NSMutableArray()// this array will be used as a filtered array
    var dictWODetails = NSDictionary()
    
    
    // MARK: - view's life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tblviewWorkOrders.tableFooterView = UIView()
        
        if(isInternetAvailable())
        {
            getAllWorkOrders()
        }
        else{
            
            showAlertWithoutAnyAction(strtitle: alertMessage, strMessage: alertInternet, viewcontrol: self)
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    // MARK: - UIButton action
 
    @IBAction func actionOnCompleted(_ sender: UIButton) {
        type = "completed"
        arrayWorkOrdersCopy.removeAllObjects()
        for item in arrayWorkOrders{
            
            if("\((item as! NSDictionary).value(forKey: "WorkorderStatus")!)" == "Completed")
            {
               arrayWorkOrdersCopy.add(item)
            }
        }
        if(arrayWorkOrdersCopy.count > 0)
        {
                self.constLeadingView.constant = sender.frame.origin.x
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                self.tblviewWorkOrders.reloadData()
            
        }
        else
        {
            arrayWorkOrdersCopy = arrayWorkOrders.mutableCopy() as! NSMutableArray
           showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnCancelled(_ sender: UIButton) {
      
        type = "cancelled"
        arrayWorkOrdersCopy.removeAllObjects()
        for item in arrayWorkOrders{
            
            if("\((item as! NSDictionary).value(forKey: "WorkorderStatus")!)" == "Cancelled")
            {
                arrayWorkOrdersCopy.add(item)
            }
        }
        if(arrayWorkOrdersCopy.count > 0)
        {
            constLeadingView.constant = sender.frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
             self.tblviewWorkOrders.reloadData()
            
        }
        else
        {
            arrayWorkOrdersCopy = arrayWorkOrders.mutableCopy() as! NSMutableArray

            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
        
    }
    
    @IBAction func actionOnReset(_ sender: UIButton) {
       
        
        type = "reset"
        arrayWorkOrdersCopy.removeAllObjects()
        for item in arrayWorkOrders{
            
            if("\((item as! NSDictionary).value(forKey: "WorkorderStatus")!)" == "Reset")
            {
                arrayWorkOrdersCopy.add(item)
            }
        }
        if(arrayWorkOrdersCopy.count > 0)
        {
            constLeadingView.constant = sender.frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.tblviewWorkOrders.reloadData()
            
        }
        else
        {
            arrayWorkOrdersCopy = arrayWorkOrders.mutableCopy() as! NSMutableArray

            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
 
    }
    
    @IBAction func actionOnUpcoming(_ sender: UIButton) {
        type = "upcoming"//Incomplete
        arrayWorkOrdersCopy.removeAllObjects()
        for item in arrayWorkOrders{
            
            if("\((item as! NSDictionary).value(forKey: "WorkorderStatus")!)" == "Incomplete")
            {
                arrayWorkOrdersCopy.add(item)
            }
        }
        if(arrayWorkOrdersCopy.count > 0)
        {
            constLeadingView.constant = sender.frame.origin.x
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            self.tblviewWorkOrders.reloadData()
            
        }
        else
        {
            arrayWorkOrdersCopy = arrayWorkOrders.mutableCopy() as! NSMutableArray

            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
    
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionOnHome(_ sender: UIButton) {
        for vc in (self.navigationController?.viewControllers)!{
            if(vc is DashboardVC)
            {
                self.navigationController?.popToViewController(vc, animated: false)
                break
            }
        }
    }
    
    @IBAction func actionOnDocumentsFooter(_ sender: UIButton) {
        
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is DocumentsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "DocumentsVC") as! DocumentsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
       
    }
    
    @IBAction func actionOnTransactions(_ sender: UIButton) {
        
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is TransactionsVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "TransactionsVC") as! TransactionsVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
    }
    
    @IBAction func actionOnPastServices(_ sender: Any) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let docVC = storyboard.instantiateViewController(withIdentifier: "WorkOrdersVC") as! WorkOrdersVC
//        self.navigationController?.pushViewController(docVC, animated: true)
    }
    
    @IBAction func actionOnTasks(_ sender: UIButton) {
        
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is TaskVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "TaskVC") as! TaskVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
    }
    
    @IBAction func actionOnFilter(_ sender: UIButton) {
        
        UIButton.animate(withDuration: 0.2,
                         animations: {
                            sender.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        },
                         completion: { finish in
                            UIButton.animate(withDuration: 0.2, animations: {
                                sender.transform = CGAffineTransform.identity
                                
                                var vcFound = false
                                for vc in (self.navigationController?.viewControllers)!{
                                    if(vc is FilterVC)
                                    {
                                        vcFound = true
                                        self.navigationController?.popToViewController(vc, animated: false)
                                        break
                                    }
                                }
                                if(vcFound == false)
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let docVC = storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
                                    self.navigationController?.pushViewController(docVC, animated: true)
                                }
                                
                            })
        })
        
        
    }
    
    // MARK: tableview cell button action
    @objc func actionOnEmployeeName(sender:UIButton!)
    {
//        let buttonPosition = sender.convert(CGPoint.zero, to: tvDashboard)
//        let indexPath = tvDashboard.indexPathForRow(at:buttonPosition)
      
        dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
      
        if("\(dictWODetails.value(forKey: "TechnicianName")!)" == "<null>" || ("\(dictWODetails.value(forKey: "TechnicianName")!)").count == 0)
        {
            // dont send to employee detail controller
        }
        else
        {
            performSegue(withIdentifier: "EmployeeProfileVC", sender: nil)
        }
       
    }
    
    @objc func actionOnOrderNumber(sender:UIButton!){
        
        dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        if("\(dictWODetails.value(forKey: "WorkOrderNo")!)" == "<null>" || ("\(dictWODetails.value(forKey: "WorkOrderNo")!)").count == 0)
        {
            // dont send to WO detail controller
        }
        else
        {
            performSegue(withIdentifier: "WODetailsVC", sender: nil)
        }
    }
    
    @objc private func actionOnSurvey(sender:UIButton)
    {
         dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
       
        if(dictWODetails.value(forKey: "SurveyURL") != nil && "\(dictWODetails.value(forKey: "SurveyURL")!)" != "<null>")
        {
            let strURL = dictWODetails.value(forKey: "SurveyURL")
            
            let url = NSURL(string: (strURL as! String).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
            
            if(url != nil)
            {
                let safariVC = SFSafariViewController(url: url!)
                safariVC.delegate = self
                self.present(safariVC, animated: true, completion: nil)
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
        }
    }
   
    @objc private func actionOnServiceComplain(sender:UIButton)
    {
        dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "SurveyComplainVC") as! SurveyComplainVC
        docVC.dictWO = dictWODetails
        docVC.strTitle =  "Service Issue"
        self.navigationController?.pushViewController(docVC, animated: true)
    }
    
    @objc private func actionOnPayBill(sender:UIButton)
    {
        print("Payment button clicked")
        dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        print(dictWODetails)
        generatePaymentToken()
    }
    @objc private func actionOnDocuments(sender:UIButton)
    {
        dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let docVC = storyboard.instantiateViewController(withIdentifier: "WODocumentsVC") as! WODocumentsVC
        docVC.dictWO = dictWODetails
        self.navigationController?.pushViewController(docVC, animated: true)
    }
    @objc func actionOnConfirm(sender:UIButton!){
        
        let dict = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        print(dict)
        callConfirmAPI(dictParam: dict)
    }
    
    @objc func actionOnReschedule(sender:UIButton!){
        
        dictWODetails = arrayWorkOrdersCopy.object(at: sender.tag) as! NSDictionary
        
        performSegue(withIdentifier: "RescheduleWorkOrderVC", sender: nil)
    }
    
    // MARK:- web service
    func getAllWorkOrders(){
        let arrayAccounts = getUserAccounts()
        print(arrayAccounts)
        if(arrayAccounts.count > 0)
        {
            for (index, element) in arrayAccounts.enumerated(){
                print("\(index)")
                print("\(element)")
                let dict = (element as! NSDictionary).mutableCopy() as! NSMutableDictionary
//                  dict.setValue("All", forKey: "WoStatus")
                dict.setValue("All", forKey: "WoStatus")

                arrayAccounts.replaceObject(at: index, with: dict)
            }
            print(arrayAccounts)
            
            if let url = NSURL(string:URL_GetWorkorderDetailForAllCompany){
                
                var request = URLRequest(url: url as URL)
                
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpMethod = "POST"
                
                //  request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
                
                
                request.httpBody = try! JSONSerialization.data(withJSONObject: arrayAccounts, options: [])
                
//                FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
                
                customDotLoaderShowOnFull(message: "", controller: self)

                Alamofire.request(request)
                    .responseJSON { response in
                        
//                        FTIndicator.dismissProgress()
                        customeDotLoaderRemove()
                        switch response.result {
                        case .success(let responseObject):
                            
                            print(responseObject)

                            self.arrayWorkOrders = (responseObject as! NSArray).mutableCopy() as! NSMutableArray
                            self.arrayWorkOrdersCopy = self.arrayWorkOrders.mutableCopy() as! NSMutableArray
                            
                            self.arrayWorkOrdersCopy.removeAllObjects()
                            for item in self.arrayWorkOrders{
                                
                                if("\((item as! NSDictionary).value(forKey: "WorkorderStatus")!)" == "Incomplete")
                                {
                                    self.arrayWorkOrdersCopy.add(item)
                                }
                            }
                            if(self.arrayWorkOrdersCopy.count > 0)
                            {
                                DispatchQueue.main.async {
                                    self.tblviewWorkOrders.reloadData()
                                }
                            }
                            else
                            {
                                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertDataNotFound, viewcontrol: self)
                            }
                        
                        case .failure(let error):
                            
                            print(error)
                        }
                }
            }
        }
    }
    func callConfirmAPI(dictParam:NSDictionary){
        
        //http://service.lifestyle.com/api/Customer/UpdateWorkOrderFlagToConfirmed?appKey=Pestream&companyKey=titan&WorkOrderNo=1355
        
        let strUrl = BaseURL+"UpdateWorkOrderFlagToConfirmed?appKey=\(dictParam.value(forKey: "AppKey")!)&companyKey=\(dictParam.value(forKey: "CompanyKey")!)&WorkOrderNo=\(dictParam.value(forKey: "WorkOrderNo")!)"
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        
        WebService.callAPIBYGETWhoseResponseIsString(parameter: NSDictionary(), url: strUrl) { (response, status) in
            
//            FTIndicator.dismissProgress()
            
            customeDotLoaderRemove()
            if(status == "success")
            {
                if(response == "True" || response == "true" || response == "TRUE")
                {
                    print(response)
                    self.getAllWorkOrders()
                }
                else{
                    showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                }
            }
            else{
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                print("some error occurred in confirm api")
            }
        }
    }
    
    func generatePaymentToken()
    {
        let dict = ["AppKey":"\(dictWODetails.value(forKey: "AppKey")!)",
            "CompanyKey":"\(dictWODetails.value(forKey: "CompanyKey")!)",
            "AccountNo":"\(dictWODetails.value(forKey: "AccountNo")!)",
            "EntityType":"Workorder",
            "EntityNumber":"\(dictWODetails.value(forKey: "WorkOrderNo")!)",
            "TransactionAmount":"\(dictWODetails.value(forKey: "TransactionAmount")!)",
            "ReturnURL":""]
        
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: dict,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.ascii) {
            print("JSON string = \n\(theJSONText)")
        }
        
//        FTIndicator.showProgress(withMessage: "please wait", userInteractionEnable: false)
        
        customDotLoaderShowOnFull(message: "", controller: self)
        
        WebService.callAPIBYPOST(parameter: dict as NSDictionary, url: URL_GeneratePaymentToken) { (response, status) in
            
//            FTIndicator.dismissProgress()
            customeDotLoaderRemove()
            
            print(status)
            print(response)
            if(status == "success")
            {
                if(("\(response.value(forKey: "data")!)").count > 0)
                {
                    
                    let strURL = "http://pestream.com/payment/GeneralPayment/Index?tokenString=" + "\(response.value(forKey: "data")!)"
                    
                    let url = NSURL(string: (strURL ).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) as URL?
                    
                    if(url != nil)
                    {
                        let safariVC = SFSafariViewController(url: url!)
                        safariVC.delegate = self
                        self.present(safariVC, animated: true, completion: nil)
                    }
                }
            }
           else
            {
                showAlertWithoutAnyAction(strtitle: "Message", strMessage: alertSomeError, viewcontrol: self)
                print("some error occurred in confirm api")
            }
           
        }
    }
    
    // MARK: - Segue Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "WODetailsVC")
        {
            let vc = segue.destination as! WODetailsVC
            vc.dictWODetails = dictWODetails
        }
        else if(segue.identifier == "EmployeeProfileVC")
        {
            let vc = segue.destination as! EmployeeProfileVC
            vc.dictData = dictWODetails
        }
        else if(segue.identifier == "RescheduleWorkOrderVC")
        {
            let vc = segue.destination as! RescheduleWorkOrderVC
            vc.dictWO = dictWODetails
        }
    }
}
extension WorkOrdersVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrayWorkOrdersCopy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cellUpcoming = tableView.dequeueReusableCell(withIdentifier: "CellUpcomingActivities") as? CellUpcomingActivities
   
        var cellCompleted = tableView.dequeueReusableCell(withIdentifier: "CellCompletedActivities") as? CellCompletedActivities
        
        if(cellUpcoming == nil)
        {
            cellUpcoming = UITableViewCell.init(style: .default, reuseIdentifier: "CellUpcomingActivities") as! CellUpcomingActivities
        }
        
        if(cellCompleted == nil)
        {
            cellCompleted = UITableViewCell.init(style: .default, reuseIdentifier: "CellCompletedActivities") as! CellCompletedActivities
        }
        
        if(type == "completed" || type == "cancelled")// completed
        {
            let dictData = arrayWorkOrdersCopy.object(at: indexPath.row) as! NSDictionary
            
            let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
            SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                
            }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                
                if image != nil {
                    
                    addAnimationOnView(view: cellCompleted!.imgViewCompanyLogo)
                    cellCompleted!.imgViewCompanyLogo.image = image
                    
                }
            })
            
            cellCompleted!.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
            
            cellCompleted!.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"

            if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
            {
                cellCompleted!.btnEmployeeName.setTitle("", for: .normal)
            }
            else
            {
                cellCompleted!.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
            }
            
            cellCompleted!.btnEmployeeName.tag = indexPath.row
            
            cellCompleted!.btnEmployeeName.addTarget(self, action: #selector(actionOnEmployeeName), for: .touchUpInside)
            
            if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
            {
                cellCompleted!.lblDateTime.text = ""
            }
            else
            {
                cellCompleted!.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
            }
            
            cellCompleted!.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
            
            cellCompleted!.btnOrderNo.tag = indexPath.row
            cellCompleted!.btnOrderNo.addTarget(self, action: #selector(actionOnOrderNumber), for: .touchUpInside)
            
            // cellCompleted.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
            
            cellCompleted!.btnSurvey.layer.cornerRadius = cellCompleted!.btnSurvey.frame.size.height/2
            cellCompleted!.btnSurvey.tag = indexPath.row
            cellCompleted!.btnSurvey.addTarget(self, action: #selector(actionOnSurvey), for: .touchUpInside)
            
            cellCompleted!.btnPay.layer.cornerRadius = cellCompleted!.btnPay.frame.size.height/2
            cellCompleted!.btnPay.tag = indexPath.row
            cellCompleted!.btnPay.addTarget(self, action: #selector(actionOnPayBill), for: .touchUpInside)
            
            cellCompleted!.btnServiceComplain.layer.cornerRadius = cellCompleted!.btnServiceComplain.frame.size.height/2
            cellCompleted!.btnServiceComplain.tag = indexPath.row
            cellCompleted!.btnServiceComplain.addTarget(self, action: #selector(actionOnServiceComplain), for: .touchUpInside)
            
            cellCompleted!.btnDocuments.layer.cornerRadius = cellCompleted!.btnDocuments.frame.size.height/2
            cellCompleted!.btnDocuments.tag = indexPath.row
            cellCompleted!.btnDocuments.addTarget(self, action: #selector(actionOnDocuments), for: .touchUpInside)
            
            if("\(dictData.value(forKey: "WorkorderStatus")!)" == "Cancelled")
            {
                cellCompleted!.btnSurvey.isHidden = true
                cellCompleted!.btnPay.isHidden = true
                cellCompleted!.btnServiceComplain.isHidden = true
                cellCompleted!.btnDocuments.isHidden = true
                cellCompleted!.viewVertical1.isHidden = true
                cellCompleted!.viewVertical2.isHidden = true
            }
            else
            {
                cellCompleted!.btnSurvey.isHidden = false
                cellCompleted!.btnPay.isHidden = false
                cellCompleted!.btnServiceComplain.isHidden = false
                cellCompleted!.btnDocuments.isHidden = false
                cellCompleted!.viewVertical1.isHidden = false
                cellCompleted!.viewVertical2.isHidden = false
            }
            
            if("\(dictData.value(forKey: "WorkorderStatus")!)" == "Completed")
            {
                if("\(dictData.value(forKey: "InvoiceStatus")!)" == "Closed" || "\(dictData.value(forKey: "InvoiceStatus")!)" == "closed")
                {
                    cellCompleted!.btnPay.isHidden = false
                    cellCompleted!.btnPay.isUserInteractionEnabled = false
                    cellCompleted!.btnPay.backgroundColor = greenColor
                    cellCompleted!.btnPay.setTitle("Paid", for: .normal)
                }
                else
                {
                    if let transactionAmount = dictData.value(forKey: "TransactionAmount") as? NSNumber {
                        let amt = transactionAmount.floatValue
                        
                        if(amt > 0.0)
                        {
                            cellCompleted!.btnPay.isHidden = false
                            cellCompleted!.btnPay.isUserInteractionEnabled = true
                            cellCompleted!.btnPay.backgroundColor = UIColor.red
                            cellCompleted!.btnPay.setTitle("Pay Bill", for: .normal)
                        }
                        else
                        {
                            cellCompleted!.btnPay.isHidden = true
                        }
                    }
                }
            }
            
            return cellCompleted!
        }
        else if(type == "upcoming" || type == "reset" ){
            
            let dictData = arrayWorkOrdersCopy.object(at: indexPath.row) as! NSDictionary
            
            let url = URL(string: "\(dictData.value(forKey: "LogoImagePath")!)")
            SDWebImageManager.shared()?.downloadImage(with: url, options: .highPriority, progress: { (receivedSize, expectedSize) in
                
            }, completed: { (image, error, cacheType, isFinished, imageUrl) in
                
                if image != nil {
                    
                    addAnimationOnView(view: cellUpcoming!.imgViewCompanyLogo)
                    cellUpcoming!.imgViewCompanyLogo.image = image
                    
                }
            })
            
            cellUpcoming!.lblCompanyName.text = "\(dictData.value(forKey: "ProviderCompanyName")!)"
          
            cellUpcoming!.lblAccNo.text = "Acc#: \(dictData.value(forKey: "AccountNo")!)"

            if("\(dictData.value(forKey: "TechnicianName")!)" == "<null>")
            {
                cellUpcoming!.btnEmployeeName.setTitle("", for: .normal)
                
            }
            else
            {
                cellUpcoming!.btnEmployeeName.setTitle("\(dictData.value(forKey: "TechnicianName")!)", for: .normal)
            }
            
            cellUpcoming!.btnEmployeeName.tag = indexPath.row
            
            cellUpcoming!.btnEmployeeName.addTarget(self, action: #selector(actionOnEmployeeName), for: .touchUpInside)
            
            if(checkNullValue(str: "\(dictData.value(forKey: "ServiceDateTime")!)")?.count == 0)
            {
                cellUpcoming!.lblDateTime.text = ""
            }
            else
            {
                cellUpcoming!.lblDateTime.text = dateTimeConvertor(str: "\(dictData.value(forKey: "ServiceDateTime")!)", formet: "MM-dd-yyyy HH:mm:ss", strFormatToBeConverted: "MM/dd/yyyy hh:mm a")
            }
            
            cellUpcoming!.btnOrderNo.setTitle("Order#:" + "\(dictData.value(forKey: "WorkOrderNo")!)" , for: .normal)
            
            cellUpcoming!.btnOrderNo.tag = indexPath.row
            cellUpcoming!.btnOrderNo.addTarget(self, action: #selector(actionOnOrderNumber), for: .touchUpInside)
            
            cellUpcoming!.btnConfirm.tag = indexPath.row
            cellUpcoming!.btnConfirm.layer.cornerRadius =  cellUpcoming!.btnConfirm.frame.size.height/2
            
            if("\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "true" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "True" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "TRUE" || "\(dictData.value(forKey: "IsCustomerConfirmed")!)" == "1")
            {
                cellUpcoming!.btnConfirm.setTitle("Confirmed", for: .normal)
                cellUpcoming!.btnConfirm.isUserInteractionEnabled = false
                cellUpcoming!.btnConfirm.alpha = 0.5
            }
            else
            {
                cellUpcoming!.btnConfirm.setTitle("Confirm", for: .normal)
                
                cellUpcoming!.btnConfirm.isUserInteractionEnabled = true
                
                cellUpcoming!.btnConfirm.addTarget(self, action: #selector(actionOnConfirm), for: .touchUpInside)
                cellUpcoming!.btnConfirm.alpha = 1
            }
            
            cellUpcoming!.btnReschedule.layer.cornerRadius =  cellUpcoming!.btnReschedule.frame.size.height/2
            cellUpcoming!.btnReschedule.tag = indexPath.row
            cellUpcoming!.btnReschedule.addTarget(self, action: #selector(actionOnReschedule), for: .touchUpInside)
            
            if("\(dictData.value(forKey: "WOOnMyWay")!)" == "true" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "True" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "TRUE" || "\(dictData.value(forKey: "WOOnMyWay")!)" == "1" && "\(dictData.value(forKey: "TimeIn")!)" == "<null>")
            {
                cellUpcoming!.btnConfirm.isHidden = true
                cellUpcoming!.btnReschedule.isHidden = true
                cellUpcoming!.contentView.backgroundColor = UIColor.yellow
            }
            else
            {
                cellUpcoming!.btnConfirm.isHidden = false
                cellUpcoming!.btnReschedule.isHidden = false
                cellUpcoming!.contentView.backgroundColor = UIColor.white
            }
            
            if("\(dictData.value(forKey: "WorkorderStatus")!)" == "Reset" || "\(dictData.value(forKey: "WorkorderStatus")!)" == "reset")
            {
                cellUpcoming!.constWidthConfirmBtn.constant = 0.0
                cellUpcoming!.constLeadingConfirmBtn.constant = 0.0
            }
            else
            {
                cellUpcoming!.constWidthConfirmBtn.constant = 100.0
                cellUpcoming!.constLeadingConfirmBtn.constant = 10.0
            }
            
            return cellUpcoming!
        }
        return cellCompleted!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
         let dictData = arrayWorkOrdersCopy.object(at: indexPath.row) as! NSDictionary
        
         if("\(dictData.value(forKey: "WorkorderStatus")!)" == "Cancelled" || "\(dictData.value(forKey: "WorkorderStatus")!)" == "cancelled")
         {
            return 135.0
         }
        
        return 160.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.6
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.3) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
}

extension WorkOrdersVC:SFSafariViewControllerDelegate{
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
        controller.dismiss(animated: true, completion: nil)
        
    }
    
}
